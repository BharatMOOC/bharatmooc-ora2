#!/usr/bin/env bash

FIXTURES=`dirname $BASH_SOURCE`
BHARATMOOC_PLATFORM='/bharatmooc/app/bharatmoocapp/bharatmooc-platform'

$BHARATMOOC_PLATFORM/manage.py lms dumpdata --settings=devstack submissions --indent 4 > $FIXTURES/submission.json
$BHARATMOOC_PLATFORM/manage.py lms dumpdata --settings=devstack assessment --indent 4 > $FIXTURES/assessments.json
$BHARATMOOC_PLATFORM/manage.py lms dumpdata --settings=devstack workflow --indent 4 > $FIXTURES/workflow.json
$BHARATMOOC_PLATFORM/manage.py lms dumpdata --settings=devstack courseware --indent 4 > $FIXTURES/courseware.json
