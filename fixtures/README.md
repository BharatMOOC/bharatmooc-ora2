Fixtures
========

Dummy courseware, users, submissions, and assessments to make it easier to proof the UI in the LMS.

These are meant to be installed in a devstack VM.
See https://github.com/bharatmooc/configuration/wiki/BharatMOOC-Developer-Stack for
detailed installation and troubleshooting instructions.

Usage
-----

1. Run the installation script.
```
cd bharatmooc-ora2
./fixtures/install.sh
```
**WARNING**: This will wipe out all student and course state before installing the fixtures.

2. Start the LMS:
```
cd bharatmooc-platform
rake devstack[lms]
```

3. Log in as user "proof@example.com" with password "bharatmooc".

4. In the "Tim" course, you will find problems in each of the available states.
**NOTE**: There are currently more problems in the course than states.


Generating fixtures
-------------------

To regenerate test fixtures (perhaps after running a database migration):
```
cd bharatmooc-ora2
./fixtures/dump.sh
```

This will create new JSON fixtures in bharatmooc-ora2/fixtures, which you can commit
to the bharatmooc-ora2 repo.
